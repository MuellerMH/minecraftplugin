package de.gmx.mailonko;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class demoplugin extends JavaPlugin implements Listener {
	
	Player player;
	
	PotionEffect WaterBreathing = new PotionEffect(PotionEffectType.WATER_BREATHING,999999,1);
	PotionEffectType WaterBreathingType = PotionEffectType.WATER_BREATHING;

	PotionEffect Hast = new PotionEffect(PotionEffectType.FAST_DIGGING,999999,1);	
	PotionEffectType HastType = PotionEffectType.FAST_DIGGING;
	
	PotionEffect Speed = new PotionEffect(PotionEffectType.SPEED,999999,1);
	PotionEffectType SpeedType = PotionEffectType.SPEED;
	
	// called on PluginLoad
	public void onEnable(){ 

        Bukkit.getPluginManager().registerEvents(this, this);
	}
	 
	//called on PluginUnload
	public void onDisable(){ 

	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) 
	{
		this.player = event.getPlayer();
		if(isPlayerInWater() 
				&& this.player.isOp() // zum testen
				//&& isPlayerPermission("packet.isZandaren")  // Was auch immer				
				&& (
				   !this.player.hasPotionEffect(WaterBreathingType) 
				|| !this.player.hasPotionEffect(HastType)
				|| !this.player.hasPotionEffect(SpeedType) 
				)
		) {
			this.player.addAttachment(this);
			this.player.addPotionEffect(WaterBreathing);
			this.player.addPotionEffect(Hast);
			this.player.addPotionEffect(Speed);
		}
		
		if( 
				this.player.hasPermission(this.getName())
				&& ( 	
						this.player.hasPotionEffect(HastType) 
						|| this.player.hasPotionEffect(SpeedType) 
						|| this.player.hasPotionEffect(WaterBreathingType)   
				   )
				&& !isPlayerInWater()				
		) {
			this.player.removePotionEffect(WaterBreathingType);
			this.player.removePotionEffect(HastType);
			this.player.removePotionEffect(SpeedType);
		}
	}
	
	public boolean isPlayerInWater()
	{
		if(this.player.getLocation().getBlock().isLiquid())
		{
			return true;
		}
		return false;
	}
	
	public boolean isPlayerPermission(String permissionName)
	{
		if(player.hasPermission(permissionName))
		{
			return true;
		}
		
		return false;
	}
}
